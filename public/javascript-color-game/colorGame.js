var difficulty = 9; //number of squares on page
var colors = []; //js is throwing errors when its not declared with = [] and then filled in loop
//fillColors(difficulty);
var squares = document.querySelectorAll(".square"); //puts all squares into an array
var randColor;
//var randColor = colors[Math.floor(Math.random()*colors.length)];
//colorSquares();
//var colorDisplay = document.getElementById("colorDisplay"); //for displaying random color rgb code to guess to user
//colorDisplay.textContent = randColor; //for displaying random color rgb code to guess to user
var messageDisplay = document.querySelector("#message"); //correct/false message
document.getElementById("reset").addEventListener("click", reset); //sets New colors! button
reset();    


function reset(){
    fillColors(difficulty); 
    colorSquares();
    document.getElementById("main").style.background = "steelblue"; //resets background to default
    randColor = colors[Math.floor(Math.random()*colors.length)];
    colorDisplay.textContent = randColor; //shows color to guess
    document.getElementById("thumbsup").style.display = "none"; //hides thumbs up loli
    document.getElementById("message").textContent = ""; //empties message
    document.getElementById("reset").textContent = "New Colors"; //set button textContent to New Colors because player didnt win yet
}


//changes background of squares to those from color array
function colorSquares(){
    for (var i=0 ; i < squares.length; i++) {
        squares[i].style.background = colors[i]; //adds background style from colors array to every square
        squares[i].addEventListener("click", checkColor);
    }
}

//changes color of all squares after correct answer
function changeColors(color){
    for (var i=0 ; i < squares.length; i++){
        squares[i].style.background = color;
    }
}

//checks if clicked square matches with random color
function checkColor(){
    var clickedColor = this.style.background
    if (clickedColor === randColor){ //comparing current square background to random color
    messageDisplay.textContent = "Correct Answer!";
    changeColors(clickedColor);
    document.getElementById("main").style.background = clickedColor; //changes color of h1
    document.getElementById("thumbsup").style.display = "block";
    document.getElementById("reset").textContent = "Play Again";
    }
    else{
    messageDisplay.textContent = "Wrong Answer!";
    this.style.background = "#232323" //changes square color to body background color to make it disappear
    }
}

//fills colors array with random colors
function fillColors(num){ 
    for (var i=0; i<num; i++){
        colors[i] = randRGBColor();
    }
}

//generates random RGB color
function randRGBColor(){
    var r = Math.floor(Math.random() * 256);
    var g = Math.floor(Math.random() * 256);
    var b = Math.floor(Math.random() * 256);
    var rgb = "rgb(" + r + ", " + g + ", " + b + ")";
    return rgb;
}