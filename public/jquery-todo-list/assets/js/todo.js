// toggles grey text color and line-through, have to use parent element to work for all future elements
$("ul").on("click", "li", function(){
    $(this).toggleClass("checked");
});

//delete button
$("ul").on("click", "span", function(){ 
    //deletes element with parent
    $(this).parent().fadeOut(700, function(){ 
        $(this).remove(); //deletes AFTER fade out
    }); 
    event.stopPropagation(); //prevents bubbling - rest of the li doesn't change by clicking on a span inside li 
});

//only text inputs
$("input[type='text']").keypress(function(event){    
    if(event.which === 13){    
        $("ul").append("<li><span><i class='fas fa-trash'></i></span>" + $(this).val() + "</li>" ); // grabs current value of input with .val() and appends it to ul
        $(this).val(""); // clears input field
    }
});